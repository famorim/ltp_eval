from setuptools import setup

setup(name='ltp_eval',
      version='0.2',
      description='Query your results',
      url='',
      author='LTP',
      author_email='franciscoamorim@ltplabs.com',
      license='LTP',
      packages=['ltp_eval'],
      zip_safe=False)